<?php
namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Fahrten Entität.
 *
 * @ApiResource(attributes={"filters"={"journey.search_filter"}})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JourneyRepository")
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class Journey
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location = '';

    /**
     * @ORM\Column(type="float", precision=10, scale=2)
     */
    private $price = 0.00;

    /**
     * @ORM\Column(type="datetime")
     */
    private $departure = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $arrival = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type = 'OUTWARD';

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $availableSeats = 0;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="journey")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $driver = null;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="ride")
     * @ORM\JoinTable(
     *     name="UserHasJourney",
     *     joinColumns={@ORM\JoinColumn(name="journey_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id2", referencedColumnName="id", nullable=false)}
     * )
     */
    private $passengers = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->passengers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Journey
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set departure
     *
     * @param DateTime $departure
     *
     * @return Journey
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;

        return $this;
    }

    /**
     * Get departure
     *
     * @return DateTime
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Journey
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set availableSeats
     *
     * @param integer $availableSeats
     *
     * @return Journey
     */
    public function setAvailableSeats($availableSeats)
    {
        $this->availableSeats = $availableSeats;

        return $this;
    }

    /**
     * Get availableSeats
     *
     * @return integer
     */
    public function getAvailableSeats()
    {
        return $this->availableSeats;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Journey
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set driver
     *
     * @param User $driver
     *
     * @return Journey
     */
    public function setDriver(User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Add passenger
     *
     * @param User $passenger
     *
     * @return Journey
     */
    public function addPassenger(User $passenger)
    {
        $this->availableSeats = $this->availableSeats + 1;
        $this->passengers[]   = $passenger;

        return $this;
    }

    /**
     * Remove passenger
     *
     * @param User $passenger
     */
    public function removePassenger(User $passenger)
    {
        $this->availableSeats = $this->availableSeats - 1;
        $this->passengers->removeElement($passenger);
    }

    /**
     * Get passengers
     *
     * @return Collection
     */
    public function getPassengers()
    {
        return $this->passengers;
    }

    /**
     * @return DateTime|null
     */
    public function getArrival()
    {
        return $this->arrival;
    }

    /**
     * @param DateTime $arrival
     *
     * @return $this
     */
    public function setArrival(DateTime $arrival)
    {
        $this->arrival = $arrival;

        return $this;
    }
}

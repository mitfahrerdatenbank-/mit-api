<?php
namespace AppBundle\Action\User;

use AppBundle\Action\AbstractAction;
use AppBundle\Entity\User;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Verarbeiten eines PUT Requests eines Benutzers.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class UserPut extends AbstractAction
{
    /** @var UserRepository */
    private $userRepository = null;

    /** @var RouterInterface */
    private $router = null;

    /**
     * Konstruktor.
     *
     * @param UserRepository           $userRepository
     * @param RouterInterface          $router
     */
    public function __construct(
        UserRepository $userRepository,
        RouterInterface $router
    )
    {
        $this->userRepository = $userRepository;
        $this->router         = $router;
    }

    /**
     * Prüft, ob Benutzer Rechte zur Änderung hat.
     *
     * @Route(
     *     name="user_put",
     *     path="/users/{id}",
     *     defaults={"_api_resource_class"=User::class, "_api_item_operation_name"="put"}
     * )
     * @Method("PUT")
     *
     * @return JsonResponse|User
     */
    public function __invoke($data)
    {
        /** @var User $data */
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        if ($user->getId() !== $data->getId())
        {
            return JsonResponseMessageFactory::build('access denied', 1510216983, Message::STATUS_DENIED);
        }

        if ($data->getEmail() !== $user->getEmail())
        {
            return JsonResponseMessageFactory::build('email cannot be changed', 1510221138, Message::STATUS_DENIED);
        }

        $rideOrJourneysAreTriedToUpdate = $this->checkIfRideOrJourneysAreTriedToUpdate($user);
        if ($rideOrJourneysAreTriedToUpdate instanceof JsonResponse)
        {
            return $rideOrJourneysAreTriedToUpdate;
        }

        return $data;
    }

    /**
     * Prüft, ob versucht wird, eine Fahrt oder Mitfahrt hinzuzufügen, wirft ansonsten einen Fehler.
     *
     * @param User $user
     *
     * @return JsonResponse
     */
    private function checkIfRideOrJourneysAreTriedToUpdate(User $user)
    {
        /** @var User $userOld */
        $userOld = $this->userRepository->findOneBy(['id' => $user->getId()]);
        if ($user->getJourney() !== $userOld->getJourney() || $user->getRide() !== $userOld->getRide())
        {
            $routeToJourneys = $this->router->generate('journey_put', ['id' => '{id}']);
            return JsonResponseMessageFactory::build(
                'journey or ride has to be changed over: ' . $routeToJourneys,
                1510304198,
                Message::STATUS_FAILED
            );
        }
    }
}
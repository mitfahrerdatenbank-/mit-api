<?php
namespace AppBundle\Registration\Interpreter\User;

use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interpretiert anhand eines Benutzer den Link zur Bestätigung der e-Mail Adresse.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class RegistrationLinkInterpreter
{
    /** @var ContainerInterface */
    private $container = null;

    /**
     * Konstruktor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Gibt den Link für die Bestätigung der e-Mail Adresse zurück.
     *
     * @param User $user
     *
     * @return string
     */
    public function interpret(User $user)
    {
        $verifyLink = $this->container->getParameter('base_url');
        $hash       = $verifyLink . 'users/' . $user->getId() . '/verify/' . $user->getRegistrationHash();

        return $hash;
    }
}
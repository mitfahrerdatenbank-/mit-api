<?php
namespace AppBundle\Action\Journey;

use ApiPlatform\Core\Serializer\ItemNormalizer;
use AppBundle\Action\AbstractAction;
use AppBundle\Entity\Journey;
use AppBundle\Entity\User;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use AppBundle\Repository\JourneyRepository;
use AppBundle\Repository\VehicleRepository;
use Happyr\MailerBundle\Services\MailerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines PUT Requests einer Fahrt.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class JourneyPut extends AbstractAction
{
    /** @var VehicleRepository */
    private $vehicleRepository = null;

    /** @var JourneyRepository */
    private $journeyRepository = null;

    /** @var MailerService */
    private $mailer = null;

    /** @var ItemNormalizer */
    private $itemNormalizer = null;

    /**
     * Konstruktor.
     *
     * @param VehicleRepository $vehicleRepository
     * @param JourneyRepository $journeyRepository
     * @param MailerService     $mailer
     * @param ItemNormalizer    $itemNormalizer
     */
    public function __construct(
        VehicleRepository $vehicleRepository,
        JourneyRepository $journeyRepository,
        MailerService $mailer,
        ItemNormalizer $itemNormalizer
    )
    {
        $this->vehicleRepository = $vehicleRepository;
        $this->journeyRepository = $journeyRepository;
        $this->mailer            = $mailer;
        $this->itemNormalizer    = $itemNormalizer;
    }

    /**
     * Aktualisiert eine Fahrt, informiert Fahrer/Mitfahrer über Änderungen per e-Mail.
     *
     * @Route(
     *     name="journey_put",
     *     path="/journeys/{id}",
     *     defaults={"_api_resource_class"=Journey::class, "_api_item_operation_name"="put"}
     * )
     * @Method("PUT")
     *
     * @return JsonResponse
     */
    public function __invoke($data)
    {
        /** @var Journey $data */
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        /** @var Journey $currentJourney */
        $currentJourney         = $this->journeyRepository->findOneBy(['id' => $data->getId()]);
        $currentPassengersCount = $currentJourney->getPassengers()->count();

        $response = $this->checkAvailableSeatsInRange($data);
        if ($response instanceof JsonResponse)
        {
            return $response;
        }

        $removedPassengers = array_diff(
            $currentJourney->getPassengers()->toArray(),
            $data->getPassengers()->toArray()
        );

        if (in_array($user, $removedPassengers, true))
        {
            $passenger = reset($removedPassengers);

            $this->sendPassengerCancelledJourneyMail($data, $passenger);
        }
        else
        {
            /** @var User $passenger */
            foreach ($removedPassengers as $passenger)
            {
                $this->sendRemovedPassengerEmail($data, $passenger);
            }
        }

        $addedPassenger = array_diff(
            $data->getPassengers()->toArray(),
            $currentJourney->getPassengers()->toArray()
        );

        if (in_array($user, $addedPassenger, true))
        {
            $passenger = reset($addedPassenger);
            $this->sendInformationAboutNewPassengerToDriver($data, $passenger);
            $this->sendConfirmationAboutBookingJourney($data, $passenger);
        }

        $updatedPassengersCount = $data->getPassengers()->count();
        if ($currentPassengersCount !== $updatedPassengersCount)
        {
            $this->adjustAvailableSeats($data);
        }

        $this->journeyRepository->save($data);

        $normalizedJourney = $this->itemNormalizer->normalize($data);

        return new JsonResponse($normalizedJourney, 201);
    }

    /**
     * Prüft, ob die Anzahl der verfügbaren Sitzplätze nicht über-, oder unterschritten wurde.
     *
     * @param Journey $updatedJourney
     *
     * @return JsonResponse
     */
    private function checkAvailableSeatsInRange(Journey $updatedJourney)
    {
        $updatedPassengersCount = $updatedJourney->getPassengers()->count();

        $updatedJourneyAvailableSeats = $updatedJourney->getAvailableSeats();
        if ($updatedPassengersCount > $updatedJourneyAvailableSeats || $updatedPassengersCount <
            $updatedJourneyAvailableSeats && $updatedPassengersCount !== 0)
        {
            return $this->throwAvailableSeatsNotInRangeResponse($updatedJourneyAvailableSeats, $updatedPassengersCount);
        }
    }

    /**
     * Gibt eine JsonResponse Fehlermeldung zurück.
     *
     * @param int $availableSeats
     * @param int $used
     *
     * @return JsonResponse
     */
    private function throwAvailableSeatsNotInRangeResponse(int $availableSeats, int $used)
    {
        return JsonResponseMessageFactory::build(
            'available seats are undershot, or exceeded. available:' . $availableSeats . ', used:' . $used,
            1510312825,
            Message::STATUS_ERROR
        );
    }

    /**
     * Passt die Anzahl der verfügbaren Sitzplätze an.
     *
     * @param Journey $updatedJourney
     *
     * @return void
     */
    private function adjustAvailableSeats(Journey $updatedJourney)
    {
        $updatedJourney->setAvailableSeats($updatedJourney->getPassengers()->count());
    }

    /**
     * Sendet eine e-Mail an den Fahrer über die Stornierung der Mitfahrt.
     *
     * @param $updatedJourney
     * @param $passenger
     *
     * @return void
     */
    private function sendPassengerCancelledJourneyMail(Journey $updatedJourney, User $passenger) : void
    {
        $mailParameters = [
            'firstName' => $updatedJourney->getDriver()->getFirstName(),
            'lastName'  => $updatedJourney->getDriver()->getLastName(),
            'departure' => $updatedJourney->getDeparture(),
            'location'  => $updatedJourney->getLocation(),
            'type'      => $updatedJourney->getType(),
            'price'     => $updatedJourney->getPrice(),
            'passenger' => [
                'firstName' => $passenger->getFirstName(),
                'lastName'  => $passenger->getLastName(),
            ]
        ];

        $this->mailer->send(
            $updatedJourney->getDriver()->getEmail(),
            '@App/Email/Journey/passenger-cancelled-journey.html.twig',
            $mailParameters
        );
    }

    /**
     * Sendet e-Mail an Fahrer über neuen Mitfahrer.
     *
     * @param Journey $updatedJourney
     * @param User    $passenger
     *
     * @return void
     */
    private function sendInformationAboutNewPassengerToDriver(Journey $updatedJourney, User $passenger)
    {
        $mailParameters = [
            'firstName' => $updatedJourney->getDriver()->getFirstName(),
            'lastName'  => $updatedJourney->getDriver()->getLastName(),
            'departure' => $updatedJourney->getDeparture(),
            'location'  => $updatedJourney->getLocation(),
            'type'      => $updatedJourney->getType(),
            'price'     => $updatedJourney->getPrice(),
            'passenger' => [
                'firstName' => $passenger->getFirstName(),
                'lastName'  => $passenger->getLastName(),
            ],
        ];

        $this->mailer->send(
            $updatedJourney->getDriver()->getEmail(),
            '@App/Email/Journey/passenger-booked-journey.html.twig',
            $mailParameters
        );
    }

    /**
     * Sendet e-Mail an Mitfahrer über Bestätigung von Mitfahrt.
     *
     * @param Journey $updatedJourney
     * @param User    $passenger
     *
     * @return void
     */
    private function sendConfirmationAboutBookingJourney(Journey $updatedJourney, User $passenger)
    {
        $mailParameters = [
            'firstName' => $updatedJourney->getDriver()->getFirstName(),
            'lastName'  => $updatedJourney->getDriver()->getLastName(),
            'departure' => $updatedJourney->getDeparture(),
            'location'  => $updatedJourney->getLocation(),
            'type'      => $updatedJourney->getType(),
            'price'     => $updatedJourney->getPrice(),
            'passenger' => [
                'firstName' => $passenger->getFirstName(),
                'lastName'  => $passenger->getLastName(),
            ],
            'driver' => [
                'firstName' => $updatedJourney->getDriver()->getFirstName(),
                'lastName'  => $updatedJourney->getDriver()->getLastName(),
            ],
        ];

        $this->mailer->send(
            $updatedJourney->getDriver()->getEmail(),
            '@App/Email/Journey/passenger-confirmation-about-booking.html.twig',
            $mailParameters
        );
    }

    /**
     * Sendet eine e-Mail über die Entfernung eines Mitfahrers von der Fahrt durch Fahrer.
     *
     * @param Journey $updatedJourney
     * @param User    $passenger
     *
     * @return void
     */
    private function sendRemovedPassengerEmail(Journey $updatedJourney, User $passenger)
    {
        $mailParameters = [
            'firstName' => $updatedJourney->getDriver()->getFirstName(),
            'lastName'  => $updatedJourney->getDriver()->getLastName(),
            'departure' => $updatedJourney->getDeparture(),
            'location'  => $updatedJourney->getLocation(),
            'type'      => $updatedJourney->getType(),
            'price'     => $updatedJourney->getPrice(),
            'passenger' => [
                'firstName' => $passenger->getFirstName(),
                'lastName'  => $passenger->getLastName(),
            ],
        ];

        $this->mailer->send(
            $updatedJourney->getDriver()->getEmail(),
            '@App/Email/Journey/driver-removed-passenger-from-journey.html.twig',
            $mailParameters
        );
    }
}
<?php
namespace AppBundle\Factory;

use AppBundle\Message\Message;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Fabrik zum Erstellen einheitlicher JsonResponse's um im Frontend einfacher mit Rückgaben umzugehen.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class JsonResponseMessageFactory
{
    private static $statusToResponseCode = [
        Message::STATUS_SUCCESS => Response::HTTP_OK,
        Message::STATUS_ERROR => Response::HTTP_BAD_REQUEST,
        Message::STATUS_FAILED => Response::HTTP_BAD_REQUEST,
        Message::STATUS_INFO => Response::HTTP_OK,
        Message::STATUS_DENIED => Response::HTTP_FORBIDDEN,
        Message::STATUS_FORBIDDEN => Response::HTTP_FORBIDDEN,
    ];

    /**
     * Erstellt eine Json Response.
     *
     * @param string $message
     * @param int    $code
     * @param string $status
     * @param null   $responseCode
     *
     * @return JsonResponse
     */
    static public function build(string $message, int $code, string $status, $responseCode = null)
    {
        $message = new Message($message, $code, $status);

        return new JsonResponse($message->toArray(), self::$statusToResponseCode[$status]);
    }
}
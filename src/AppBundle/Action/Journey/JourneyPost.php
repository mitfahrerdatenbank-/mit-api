<?php
namespace AppBundle\Action\Journey;

use ApiPlatform\Core\Serializer\ItemNormalizer;
use AppBundle\Action\AbstractAction;
use AppBundle\Entity\Journey;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use AppBundle\Repository\JourneyRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines POST Requests einer Fahrt.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class JourneyPost extends AbstractAction
{
    /** @var JourneyRepository */
    private $journeyRepository = null;

    /** @var ItemNormalizer */
    private $itemNormalizer = null;

    /**
     * Konstruktor.
     *
     * @param JourneyRepository $journeyRepository
     * @param ItemNormalizer    $itemNormalizer
     */
    public function __construct(
        JourneyRepository $journeyRepository,
        ItemNormalizer $itemNormalizer
    )
    {
        $this->journeyRepository = $journeyRepository;
        $this->itemNormalizer    = $itemNormalizer;
    }

    /**
     * Speichert eine neue Fahrt ab.
     *
     * @Route(
     *     name="journey_post",
     *     path="/journeys",
     *     defaults={"_api_resource_class"=Journey::class, "_api_collection_operation_name"="post"}
     * )
     * @Method("POST")
     *
     * @return JsonResponse
     */
    public function __invoke($data)
    {
        /** @var Journey $data */
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        if ($data->getDriver() !== $user && $data->getDriver() !== null)
        {
            return JsonResponseMessageFactory::build(
                'journey can only be created for logged in user',
                1510309278,
                Message::STATUS_FORBIDDEN
            );
        }

        $data->setDriver($user);
        $this->journeyRepository->save($data);

        $normalizedJourney = $this->itemNormalizer->normalize($data);

        return new JsonResponse($normalizedJourney, 201);
    }
}
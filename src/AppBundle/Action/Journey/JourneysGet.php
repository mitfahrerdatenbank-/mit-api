<?php
namespace AppBundle\Action\Journey;

use AppBundle\Action\AbstractAction;
use AppBundle\Entity\Journey;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines GET Requests aller Fahrten.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class JourneysGet extends AbstractAction
{
    /** @var ContainerInterface */
    private $container = null;

    /** @var string[] */
    private $availableFilterKeys = ['location', 'departure', 'type'];

    /**
     * Konstruktor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Bezieht alle Fahrten, sofern Zugriff auf diese durch den Benutzer erlaubt ist.
     *
     * @Route(
     *     name="journeys_get",
     *     path="/journeys",
     *     defaults={"_api_resource_class"=Journey::class, "_api_collection_operation_name"="get"}
     * )
     * @Method("GET")
     *
     * @return array
     */
    public function __invoke($data)
    {
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        $myJourneys = [];
        /** @var Journey $journey */
        foreach ($data as $journey)
        {
            $userIsDriver    = $this->userIsDriver($journey, $user);
            $userIsPassenger = $this->userIsPassenger($journey, $user);

            $queryParameters          = $this->container->get('request_stack')->getMasterRequest()->query->all();
            $queryParametersArrayKeys = array_keys($queryParameters);
            $isSearch                 = !empty(array_intersect($queryParametersArrayKeys, $this->availableFilterKeys));

            if ($userIsDriver === true || $userIsPassenger === true || $isSearch === true)
            {
                $myJourneys[] = $journey;
            }
        }

        return $myJourneys;
    }

    /**
     * Prüft, ob der Benutzer Fahrer einer Fahrt ist.
     *
     * @param Journey $journey
     * @param User    $user
     *
     * @return bool
     */
    private function userIsDriver(Journey $journey, User $user)
    {
        return $journey->getDriver()->getId() === $user->getId();
    }

    /**
     * Prüft, ob der Benutzer Mitfahrer einer Fahrt ist.
     *
     * @param Journey $journey
     * @param User    $user
     *
     * @return bool
     */
    private function userIsPassenger(Journey $journey, User $user)
    {
        return $journey->getPassengers()->contains($user);
    }
}
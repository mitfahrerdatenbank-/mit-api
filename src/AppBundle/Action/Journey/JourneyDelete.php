<?php
namespace AppBundle\Action\Journey;

use AppBundle\Action\AbstractAction;
use AppBundle\Entity\Journey;
use AppBundle\Entity\User;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use AppBundle\Repository\JourneyRepository;
use Happyr\MailerBundle\Services\MailerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines DELETE Requests einer Fahrt.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class JourneyDelete extends AbstractAction
{
    /** @var JourneyRepository */
    private $journeyRepository = null;

    /** @var MailerService */
    private $mailer = null;

    /**
     * Konstruktor.
     *
     * @param JourneyRepository $journeyRepository
     * @param MailerService     $mailer
     */
    public function __construct(
        JourneyRepository $journeyRepository,
        MailerService $mailer
    )
    {
        $this->journeyRepository = $journeyRepository;
        $this->mailer            = $mailer;
    }

    /**
     * Verarbeitet die Löschung einer Fahrt, und informiert alle Mitfahrer über e-Mail darüber.
     *
     * @Route(
     *     name="journey_delete",
     *     path="/journeys/{id}",
     *     defaults={"_api_resource_class"=Journey::class, "_api_item_operation_name"="delete"}
     * )
     * @Method("DELETE")
     *
     * @return Response
     */
    public function __invoke($journey)
    {
        /** @var Journey $journey */
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        if ($journey->getDriver()->getId() !== $user->getId())
        {
            return JsonResponseMessageFactory::build(
                'journey can only be deleted for logged in user',
                1510309279,
                Message::STATUS_FORBIDDEN
            );
        }

        /** @var User $passenger */
        foreach ($journey->getPassengers() as $passenger)
        {
            $this->sendMail($passenger, $journey);
        }

        $this->journeyRepository->remove($journey);
        $this->journeyRepository->flush($journey);

        return new Response('', 204);
    }

    /**
     * Versendet eine e-Mail an einen Mitfahrer.
     *
     * @param User    $passenger
     * @param Journey $journey
     */
    private function sendMail(User $passenger, Journey $journey)
    {
        $mailParameters = [
            'firstName' => $passenger->getFirstName(),
            'lastName'  => $passenger->getLastName(),
            'departure' => $journey->getDeparture(),
            'location'  => $journey->getLocation(),
            'type'      => $journey->getType(),
            'price'     => $journey->getPrice(),
            'driver'    => [
                'firstName' => $journey->getDriver()->getFirstName(),
                'lastName' => $journey->getDriver()->getLastName(),
            ]
        ];

        $this->mailer->send($passenger->getEmail(), '@App/Email/Journey/delete.html.twig', $mailParameters);
    }
}
#!/bin/bash
mkdir ./web/doc
bin/console api:swagger:export > swagger.yml
spectacle swagger.yml -d ./web/doc/

echo 'Api Html Doc exported to ./web/doc/'
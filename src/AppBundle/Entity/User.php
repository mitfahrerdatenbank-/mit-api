<?php
namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Benutzer Entität.
 *
 * @ApiResource(collectionOperations={})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"get_user"})
     */
    private $id = 0;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"get_user"})
     */
    private $firstName = '';

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"get_user"})
     */
    private $lastName = '';

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"get_user"})
     */
    private $email = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"get_user"})
     */
    private $mobileNumber = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $registrationHash = '';

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Groups({"get_user"})
     */
    private $active = false;

    /**
     * @ORM\OneToOne(targetEntity="Vehicle", inversedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="vehicle_id", referencedColumnName="id", unique=true)
     * @Groups({"get_user"})
     */
    private $vehicle = null;

    /**
     * @ORM\OneToMany(targetEntity="Journey", mappedBy="driver")
     * @Groups({"get_user"})
     */
    private $journey = null;

    /**
     * @ORM\ManyToMany(targetEntity="Journey", mappedBy="passengers")
     * @Groups({"get_user"})
     */
    private $ride = null;

    /**
     * Konstruktor.
     */
    public function __construct()
    {
        $this->journey = new ArrayCollection();
        $this->ride    = new ArrayCollection();
        $this->vehicle = new Vehicle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = md5($password);

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set vehicle
     *
     * @param Vehicle $vehicle
     *
     * @return User
     */
    public function setVehicle(Vehicle $vehicle = null)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return Vehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Add journey
     *
     * @param Journey $journey
     *
     * @return User
     */
    public function addJourney(Journey $journey)
    {
        $this->journey[] = $journey;

        return $this;
    }

    /**
     * Remove journey
     *
     * @param Journey $journey
     */
    public function removeJourney(Journey $journey)
    {
        $this->journey->removeElement($journey);
    }

    /**
     * Get journey
     *
     * @return Collection
     */
    public function getJourney()
    {
        return $this->journey;
    }

    /**
     * Add ride
     *
     * @param Journey $ride
     *
     * @return User
     */
    public function addRide(Journey $ride)
    {
        $this->ride[] = $ride;

        return $this;
    }

    /**
     * Remove ride
     *
     * @param Journey $ride
     */
    public function removeRide(Journey $ride)
    {
        $this->ride->removeElement($ride);
    }

    /**
     * Get ride
     *
     * @return Collection
     */
    public function getRide()
    {
        return $this->ride;
    }

    /**
     * @return string
     */
    public function getMobileNumber() : string
    {
        return $this->mobileNumber;
    }

    /**
     * @param string $mobileNumber
     *
     * @return $this
     */
    public function setMobileNumber(string $mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationHash() : string
    {
        return $this->registrationHash;
    }

    /**
     * Generiert den Registrierungshash.
     *
     * @return string
     */
    public function generateRegistrationHash()
    {
        $this->registrationHash = md5(time() . $this->getMobileNumber() . $this->getEmail());

        return $this->registrationHash;
    }

    /**
     * Prüft, ob der Benutzer aktiv ist.
     *
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return $this
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        $this->getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * String representation of object
     * @link  http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(
            [$this->id, $this->email, $this->password, $this->active,]
        );
    }

    /**
     * Constructs the object
     * @link  http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     *
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->id, $this->email, $this->password, $this->active,
            ) = unserialize($serialized);
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->isActive();
    }
}

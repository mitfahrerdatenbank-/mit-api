<?php
namespace AppBundle\Security\Accessor;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Klasse, zum Erhalten des aktuellen Benutzers.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class UserTokenStorageAccessor
{
    /** @var TokenStorageInterface */
    private $tokenStorage = null;

    /** @var UserRepository */
    private $userRepository = null;

    /**
     * Konstruktor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage, UserRepository $userRepository)
    {
        $this->tokenStorage   = $tokenStorage;
        $this->userRepository = $userRepository;
    }

    /**
     * Liefert den aktuell eingeloggten Benutzer zurück.
     *
     * @return User|null
     */
    public function getCurrentUser()
    {
        $token = $this->tokenStorage->getToken();
        if ($token instanceof TokenInterface)
        {
            /** @var User $user */
            $user = $token->getUser();

            /** @var User $currentUser */
            $currentUser = $this->userRepository->findOneBy(['id' => $user->getId()]);

            return $currentUser;
        }

        return null;
    }
}
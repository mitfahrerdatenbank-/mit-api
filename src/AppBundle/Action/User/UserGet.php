<?php
namespace AppBundle\Action\User;

use AppBundle\Action\AbstractAction;
use AppBundle\Entity\User;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use AppBundle\Security\Service\UserHasAccessToOtherUsers;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines GET Requests eines Benutzers.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class UserGet extends AbstractAction
{
    /** @var UserHasAccessToOtherUsers */
    private $userHasAccessToOtherUsers = null;

    /**
     * Konstruktor.
     *
     * @param UserHasAccessToOtherUsers $userHasAccessToOtherUsers
     */
    public function __construct(UserHasAccessToOtherUsers $userHasAccessToOtherUsers)
    {
        $this->userHasAccessToOtherUsers = $userHasAccessToOtherUsers;
    }

    /**
     * Gibt einen Benutzer zurück, sofern der eingeloggte Benutzer dazu berechtigt ist.
     *
     * @Route(
     *     name="user_get",
     *     path="/users/{id}",
     *     defaults={"_api_resource_class"=User::class, "_api_item_operation_name"="get"}
     * )
     * @Method("GET")
     *
     * @return JsonResponse|User
     */
    public function __invoke($data)
    {
        /** @var User $data */
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        if ($user->getId() !== $data->getId() && $this->userHasAccessToOtherUsers->check($data) === false)
        {
            return JsonResponseMessageFactory::build('access denied', 1510216983, Message::STATUS_DENIED);
        }

        return $data;
    }
}
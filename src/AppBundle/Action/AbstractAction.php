<?php
namespace AppBundle\Action;

use AppBundle\Security\Accessor\UserTokenStorageAccessor;

/**
 * Abstrakte Action, mit Definierung des UserTokenStorageAccessor.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
abstract class AbstractAction
{
    /** @var UserTokenStorageAccessor */
    protected $userTokenStorageAccessor = null;

    /**
     * Setzt den UserTokenStorageAccessor Service.
     *
     * @param UserTokenStorageAccessor $userTokenStorageAccessor
     */
    public function setUserTokenStorageAccessor(UserTokenStorageAccessor $userTokenStorageAccessor)
    {
        $this->userTokenStorageAccessor = $userTokenStorageAccessor;
    }
}
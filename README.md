#Mitfahrerdatenbank API v1.0.0

##Voraussetzungen
Zum Betrieb der Applikation wird mindestens ein Webserver mit PHP7.1 und MySQL 5.6 vorausgesetzt.

##Dokumentation
Eine Dokumentation zur Schnittstelle findet man unter: ./web/doc/index.html





#
Author: Kevin Szymura <kevin@wzr.es>
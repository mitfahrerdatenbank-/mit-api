<?php
namespace AppBundle\Security\Service;

use AppBundle\Entity\Journey;
use AppBundle\Entity\User;
use AppBundle\Security\Accessor\UserTokenStorageAccessor;

/**
 * Prüft, ob der aufgerufene Benutzer in den eigenen (Mit)fahrten existiert, um Details aufrufen zu können.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class UserHasAccessToOtherUsers
{
    /** @var UserTokenStorageAccessor */
    private $userTokenStorageAccessor = null;

    /**
     * Konstruktor.
     *
     * @param UserTokenStorageAccessor $userTokenStorageAccessor
     */
    public function __construct(UserTokenStorageAccessor $userTokenStorageAccessor)
    {
        $this->userTokenStorageAccessor = $userTokenStorageAccessor;
    }

    /**
     * Prüft, ob der aufgerufene Benutzer in den eigenen (Mit)fahrten existiert, um Details aufrufen zu können.
     *
     * @param User $requestedUser
     *
     * @return bool
     */
    public function check(User $requestedUser)
    {
        $loggedInUser = $this->userTokenStorageAccessor->getCurrentUser();
        $hasAccess    = false;

        $myRides    = $loggedInUser->getRide();
        $myJourneys = $loggedInUser->getJourney();

        /** @var Journey $journey */
        foreach ($myJourneys as $journey)
        {
            if ($hasAccess)
            {
                continue;
            }
            $hasAccess = $journey->getPassengers()->contains($requestedUser);
        }

        if ($hasAccess === false)
        {
            /** @var Journey $ride */
            foreach ($myRides as $ride)
            {
                if ($hasAccess)
                {
                    continue;
                }
                $hasAccess = $ride->getDriver()->getId() === $requestedUser->getId();
            }
        }

        return $hasAccess;
    }
}
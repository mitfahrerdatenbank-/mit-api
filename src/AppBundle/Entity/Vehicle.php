<?php
namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Fahrzeug Informationen Entität.
 *
 * @ApiResource()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VehicleRepository")
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class Vehicle
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $make = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $model = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $licensePlate = '';

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="vehicle", cascade={"persist"})
     */
    private $user = null;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Vehicle
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set make
     *
     * @param string $make
     *
     * @return Vehicle
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get make
     *
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Vehicle
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set licensePlate
     *
     * @param string $licensePlate
     *
     * @return Vehicle
     */
    public function setLicensePlate($licensePlate)
    {
        $this->licensePlate = $licensePlate;

        return $this;
    }

    /**
     * Get licensePlate
     *
     * @return string
     */
    public function getLicensePlate()
    {
        return $this->licensePlate;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Vehicle
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}

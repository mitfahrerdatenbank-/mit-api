<?php
namespace AppBundle\Action\User;

use AppBundle\Action\AbstractAction;
use AppBundle\Entity\User;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Registration\Interpreter\User\RegistrationLinkInterpreter;
use AppBundle\Message\Message;
use AppBundle\Repository\UserRepository;
use Happyr\MailerBundle\Services\MailerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines POST Requests zur Registrierung eines Benutzers.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class UserRegistration extends AbstractAction
{
    /** @var UserRepository */
    private $userRepository = null;

    /** @var MailerService */
    private $mailer = null;

    /** @var RegistrationLinkInterpreter */
    private $registrationLinkInterpreter = null;

    /**
     * Konstruktor.
     *
     * @param UserRepository              $userRepository
     * @param MailerService               $mailer
     * @param RegistrationLinkInterpreter $registrationLinkInterpreter
     */
    public function __construct(
        UserRepository $userRepository,
        MailerService $mailer,
        RegistrationLinkInterpreter $registrationLinkInterpreter
    )
    {
        $this->userRepository              = $userRepository;
        $this->mailer                      = $mailer;
        $this->registrationLinkInterpreter = $registrationLinkInterpreter;
    }

    /**
     * Führt den Registrierungsprozess eines Benutzers durch.
     *
     * @Route(
     *     name="user_registration",
     *     path="/users/register",
     *     defaults={"_api_resource_class"=User::class, "_api_collection_operation_name"="register"}
     * )
     * @Method("POST")
     *
     * @param $data
     * @return User|JsonResponse
     */
    public function __invoke($data)
    {
        /** @var $data User */
        $userExists = $this->userExists($data);
        if ($userExists instanceof JsonResponse)
        {
            return $userExists;
        }

        $data->generateRegistrationHash();
        $this->userRepository->save($data);

        $this->sendRegistrationMail($data);

        return JsonResponseMessageFactory::build('registration success', 1510167385, Message::STATUS_SUCCESS);
    }

    /**
     * Prüft, ob der Benutzer bereits in der Datenbank hinterlegt ist.
     *
     * @param User $user
     * @return bool|JsonResponse
     */
    private function userExists(User $user)
    {
        $searchedUser = $this->userRepository->findOneBy(['email' => $user->getEmail()]);
        if ($searchedUser instanceof User)
        {
            return JsonResponseMessageFactory::build('user already exists', 1510137153, Message::STATUS_FAILED);
        }

        return false;
    }

    /**
     * Sendet eine Verifizierungs e-Mail an den Benutzer.
     *
     * @param User $user
     */
    private function sendRegistrationMail(User $user)
    {
        $mailParameters = [
            'firstName'  => $user->getFirstName(),
            'lastName'   => $user->getLastName(),
            'verifyLink' => $this->registrationLinkInterpreter->interpret($user),
        ];

        $this->mailer->send(
            $user->getEmail(),
            '@App/Email/User/verify-email.html.twig',
            $mailParameters
        );
    }
}
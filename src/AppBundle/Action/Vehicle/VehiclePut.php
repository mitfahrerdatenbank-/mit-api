<?php
namespace AppBundle\Action\Vehicle;

use ApiPlatform\Core\Serializer\ItemNormalizer;
use AppBundle\Action\AbstractAction;
use AppBundle\Entity\Vehicle;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use AppBundle\Repository\VehicleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines PUT Requests von Fahrzeug Informationen.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class VehiclePut extends AbstractAction
{
    /** @var VehicleRepository */
    private $vehicleRepository = null;

    /** @var ItemNormalizer */
    private $itemNormalizer = null;

    /**
     * Konstruktor.
     *
     * @param VehicleRepository $vehicleRepository
     * @param ItemNormalizer    $itemNormalizer
     */
    public function __construct(
        VehicleRepository $vehicleRepository,
        ItemNormalizer $itemNormalizer
    )
    {
        $this->vehicleRepository = $vehicleRepository;
        $this->itemNormalizer    = $itemNormalizer;
    }

    /**
     * Prüft, ob die zu aktualisierenden Fahrzeug Informationen durch den Benutzer aktualisiert werden dürfen und
     * aktualisiert diese anschließend.
     *
     * @Route(
     *     name="vehicle_put",
     *     path="/vehicle/{id}",
     *     defaults={"_api_resource_class"=Vehicle::class, "_api_item_operation_name"="put"}
     * )
     * @Method("PUT")
     *
     * @return Vehicle|JsonResponse
     */
    public function __invoke($data)
    {
        /** @var Vehicle $data */
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        if ($data->getId() !== $user->getVehicle()->getId())
        {
            return JsonResponseMessageFactory::build('access denied', 1510216989, Message::STATUS_DENIED);
        }

        if ($data->getUser()->getId() !== $user->getId())
        {
            return JsonResponseMessageFactory::build('user cannot be changed', 1510216911, Message::STATUS_FORBIDDEN);
        }

        $this->vehicleRepository->save($data);

        $normalizedVehicle = $this->itemNormalizer->normalize($data);
        unset($normalizedVehicle['user']);

        return new JsonResponse($normalizedVehicle, 200);
    }
}
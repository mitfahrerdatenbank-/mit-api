<?php
namespace AppBundle\Message;

/**
 * Klasse für Nachrichten.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class Message
{
    /** @var string */
    protected $message = '';

    /** @var int|null */
    protected $code = null;

    /** @var string */
    protected $status = self::STATUS_ERROR;

    /** @var string */
    public const STATUS_ERROR = 'ERROR';

    /** @var string */
    public const STATUS_FAILED = 'FAILED';

    /** @var string */
    public const STATUS_SUCCESS = 'SUCCESS';

    /** @var string */
    public const STATUS_INFO = 'INFO';

    /** @var string */
    public const STATUS_DENIED = 'DENIED';

    /** @var string */
    public const STATUS_FORBIDDEN = 'FORBIDDEN';

    /**
     * Konstruktor.
     *
     * @param        $message
     * @param        $code
     * @param string $status
     */
    public function __construct($message, $code, $status = self::STATUS_ERROR)
    {
        $this->message = $message;
        $this->code    = $code;
        $this->status  = $status;
    }

    /**
     * Liefert die Nachricht als Array zurück.
     *
     * @return array
     */
    public function toArray()
    {
        $data = ['message' => $this->message, 'code' => $this->code, 'status' => $this->status,];

        return $data;
    }
}
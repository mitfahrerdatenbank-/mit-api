<?php
namespace AppBundle\Security\EventListener;

use AppBundle\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * EventListener, der beim erfolgreichen Login zum AccessToken noch die Benutzerid hinzufügt.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class AuthenticationSuccessListener
{
    /**
     * Fügt die Benutzer Id zur Authentifizierungsresponse hinzu.
     *
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        /** @var User $user */
        $user = $event->getUser();

        if (!$user instanceof UserInterface)
        {
            return;
        }

        $data['data'] = ['user_id' => $user->getId(),];

        $event->setData($data);
    }
}
<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Abstrakte Basisklasse für Doctrine Repositories, die Speicher-, Merge- und Löschfunktionalität sowie den
 * entsprechenden EntityManager bereitstellt.
 *
 * @author Kevin Szymura <kevin.szymura@sh.de>
 */
abstract class AbstractRepository extends EntityRepository
{
    /**
     * Persistiert Entitäten mit einem nicht MANAGED Status.
     *
     * @see http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/working-with-objects.html#entity-state
     *
     * @param object $entity
     *
     * @return void
     */
    public function persist($entity)
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     * Mergt eine Entität.
     *
     * @param object $entity
     *
     * @return void
     */
    public function merge($entity)
    {
        $this->getEntityManager()->merge($entity);
    }

    /**
     * Löscht eine Entität.
     *
     * @param object $entity
     *
     * @return void
     */
    public function remove($entity)
    {
        $this->getEntityManager()->remove($entity);
    }

    /**
     * Flusht eine Entität.
     *
     * @param object $entity
     *
     * @return void
     */
    public function flush($entity = null)
    {
        $this->getEntityManager()->flush($entity);
    }

    /**
     * Speichert eine Entität.
     *
     * @param object $entity
     *
     * @return void
     */
    public function save($entity)
    {
        $this->persist($entity);
        $this->flush($entity);
    }

    /**
     * Gibt den EntityManager des Repositorys zurück.
     * Sollte keine EntityManager Verbindung geöffnet sein, wird eine erstellt.
     *
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        if ($this->_em->isOpen() !== true)
        {
            $this->_em = $this->_em->create($this->_em->getConnection(), $this->_em->getConfiguration());
        }

        return $this->_em;
    }
}
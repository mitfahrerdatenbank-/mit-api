<?php
namespace AppBundle\Action\Journey;

use AppBundle\Action\AbstractAction;
use AppBundle\Entity\Journey;
use AppBundle\Entity\User;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines GET Requests einer Fahrt.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class JourneyGet extends AbstractAction
{
    /**
     * Prüft, ob die Fahrt ausgegeben werden darf.
     *
     * @Route(
     *     name="journey_get",
     *     path="/journeys/{id}",
     *     defaults={"_api_resource_class"=Journey::class, "_api_item_operation_name"="get"}
     * )
     * @Method("GET")
     *
     * @return Journey|JsonResponse
     */
    public function __invoke($data)
    {
        /** @var Journey $data */
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        $userIsDriver    = $this->userIsDriver($data, $user);
        $userIsPassenger = $this->userIsPassenger($data, $user);

        if ($userIsDriver === false && $userIsPassenger === false)
        {
            return JsonResponseMessageFactory::build(
                'access denied, user is not passenger or driver',
                1510265734,
                Message::STATUS_FORBIDDEN
            );
        }

        return $data;
    }

    /**
     * Prüft, ob der Benutzer Fahrer einer Fahrt ist.
     *
     * @param Journey $journey
     * @param User    $user
     *
     * @return bool
     */
    private function userIsDriver(Journey $journey, User $user)
    {
        return $journey->getDriver()->getId() === $user->getId();
    }

    /**
     * Prüft, ob der Benutzer Mitfahrer einer Fahrt ist.
     * @param Journey $journey
     * @param User    $user
     *
     * @return bool
     */
    private function userIsPassenger(Journey $journey, User $user)
    {
        return $journey->getPassengers()->contains($user);
    }
}
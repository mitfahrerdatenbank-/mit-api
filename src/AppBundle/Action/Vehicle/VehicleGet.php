<?php
namespace AppBundle\Action\Vehicle;

use ApiPlatform\Core\Serializer\ItemNormalizer;
use AppBundle\Action\AbstractAction;
use AppBundle\Entity\User;
use AppBundle\Entity\Vehicle;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use AppBundle\Repository\UserRepository;
use AppBundle\Security\Service\UserHasAccessToOtherUsers;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines GET Requests von Fahrzeug Informationen.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class VehicleGet extends AbstractAction
{
    /** @var UserRepository */
    private $userRepository = null;

    /** @var ItemNormalizer */
    private $itemNormalizer = null;

    /** @var UserHasAccessToOtherUsers */
    private $userHasAccessToOtherUsers = null;

    /**
     * Konstruktor.
     *
     * @param UserRepository            $userRepository
     * @param ItemNormalizer            $itemNormalizer
     * @param UserHasAccessToOtherUsers $userHasAccessToOtherUsers
     */
    public function __construct(
        UserRepository $userRepository,
        ItemNormalizer $itemNormalizer,
        UserHasAccessToOtherUsers $userHasAccessToOtherUsers
    )
    {
        $this->userRepository            = $userRepository;
        $this->itemNormalizer            = $itemNormalizer;
        $this->userHasAccessToOtherUsers = $userHasAccessToOtherUsers;
    }

    /**
     * Prüft, ob der eingeloggte Benutzer Rechte zum Abrufen des angefragten Benutzers.
     *
     * @Route(
     *     name="vehicle_get",
     *     path="/vehicle/{id}",
     *     defaults={"_api_resource_class"=Vehicle::class, "_api_item_operation_name"="get"}
     * )
     * @Method("GET")
     *
     * @return JsonResponse
     */
    public function __invoke($data)
    {
        /** @var Vehicle $data */
        $user = $this->userTokenStorageAccessor->getCurrentUser();

        if ($user->getId() !== $data->getUser()->getId() &&
            $this->userHasAccessToOtherUsers->check($data->getUser()) === false
        )
        {
            return JsonResponseMessageFactory::build('access denied', 1510216983, Message::STATUS_DENIED);
        }

        $normalizedVehicle = $this->itemNormalizer->normalize($data);
        unset($normalizedVehicle['user']);

        return new JsonResponse($normalizedVehicle, 200);
    }
}
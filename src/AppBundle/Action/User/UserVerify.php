<?php
namespace AppBundle\Action\User;

use AppBundle\Action\AbstractAction;
use AppBundle\Entity\User;
use AppBundle\Factory\JsonResponseMessageFactory;
use AppBundle\Message\Message;
use AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Verarbeiten eines GET Requests zur Verifizierung der e-Mail Adresse eines Benutzers.
 *
 * @author Kevin Szymura <kevin@wzr.es>
 */
class UserVerify extends AbstractAction
{
    /** @var UserRepository */
    private $userRepository = null;

    /**
     * Konstruktor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Verifiziert einen Benutzer und aktiviert diesen.V
     *
     * @Route(
     *     name="user_verify",
     *     path="/users/{id}/verify/{registrationHash}",
     *     defaults={"_api_resource_class"=User::class, "_api_item_operation_name"="verify"}
     * )
     * @Method("GET")
     *
     * @return JsonResponse
     */
    public function __invoke($data)
    {
        /** @var $data User */
        if ($data->isActive())
        {
            return JsonResponseMessageFactory::build('user already verified', 1510141570, Message::STATUS_INFO);
        }

        $data->setActive(true);
        $this->userRepository->save($data);

        return new Symfony\Component\HttpFoundation\RedirectResponse('http://localhost:8101');

        return JsonResponseMessageFactory::build('user successfully verified', 1510221137, Message::STATUS_SUCCESS);
    }
}